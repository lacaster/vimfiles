
" -----------------------------------------------------------
" [ Vundle ]


set nocompatible              " be iMproved, necessário
filetype off                  " necessário

" define caminho do runtime para incluir Vundle e inicializá-lo

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" deixe Vundle gerenciar Vundle, necessário
Plugin 'VundleVim/Vundle.vim'

" Plugins
Plugin 'scrooloose/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'itchyny/vim-gitbranch'
Plugin 'junegunn/goyo.vim'
Plugin 'ap/vim-css-color'


" Plugins - parte 2Plug
Plugin 'scrooloose/nerdcommenter'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'mg979/vim-visual-multi'
Plugin 'tpope/vim-surround'
Plugin 'elzr/vim-json'
Plugin 'mattn/emmet-vim'
Plugin 'SirVer/ultisnips'
Plugin 'algotech/ultisnips-php'
Plugin 'algotech/ultisnips-javascript'
Plugin 'iloginow/vim-stylus'

" extra
Plugin 'airblade/vim-gitgutter'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'ervandew/supertab'

" good old plugins
Plugin 'othree/yajs.vim'
Plugin 'othree/javascript-libraries-syntax.vim'
Plugin 'moll/vim-node'
Plugin 'honza/vim-snippets'
Plugin 'noahfrederick/vim-laravel'
Plugin 'leafOfTree/vim-vue-plugin'
Plugin 'alpaca-tc/beautify.vim'
Plugin 'sophacles/vim-processing'
Plugin 'vim-scripts/vim-auto-save'
Plugin 'cakebaker/scss-syntax.vim'
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'
Plugin 'chiel92/vim-autoformat'
Plugin 'maksimr/vim-jsbeautify'
Plugin 'pangloss/vim-javascript'
Plugin 'sheerun/vim-polyglot'
Plugin 'shawncplus/phpcomplete.vim'
Plugin 'prettier/vim-prettier'

" godot
Plugin 'habamax/vim-godot'

" react
Plugin 'yuezk/vim-js'
Plugin 'maxmellon/vim-jsx-pretty'
Plugin 'epilande/vim-es2015-snippets'

" styled components
Plugin 'styled-components/vim-styled-components'

" manuais
Plugin 'alvan/vim-php-manual'

" orgmode alternatives
Plugin 'vimwiki/vimwiki'

" livereload
Plugin 'turbio/bracey.vim'

" markdown
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'

" calc
Plugin 'fedorenchik/calculator.vim'

" SML
Plugin 'jez/vim-better-sml'

" Ale
"Plugin 'dense-analysis/ale'


" Syntastic
Plugin 'vim-syntastic/syntastic'
Plugin 'mtscout6/syntastic-local-eslint.vim'

" Float term
Plugin 'voldikss/vim-floaterm'

" Split Line
Plugin 'drzel/vim-split-line'

" PHP
" Plugin 'vim-vdebug/vdebug'
" Plugin 'phpactor/phpactor'
" Plugin 'neoclide/coc.nvim', {'branch': 'release'}

" Git
Plugin 'tpope/vim-fugitive'

" Indentation
Plugin 'andersoncustodio/vim-enter-indent'
Plugin 'Vimjas/vim-python-pep8-indent'
Plugin 'vim-python/python-syntax'

" SageMath
Plugin 'petRUShka/vim-sage'

" Latex
Plugin 'xuhdev/vim-latex-live-preview'

" Spreadsheets
Plugin 'torresjrjr/tabulae.vim'

" Java
Plugin 'tpope/vim-classpath'
Plugin 'rudes/vim-java'

call vundle#end()

filetype plugin on
filetype plugin indent on


" pega referência da pasta ~/.vim
let g:vimDir = $HOME.'/.vim'

" pega referência dos arquivos:
" - settings.vim
" - mappings.vim
" - plugins.vim
let s:configSettings = g:vimDir.'/config/settings.vim'
let s:configMappings = g:vimDir.'/config/mappings.vim'
let s:configPlugins = g:vimDir.'/config/plugins.vim'
let s:configTheme = g:vimDir.'/config/theme.vim'
let s:extraSyntax = g:vimDir.'/extra/syntax/init.vim'
let s:extraScripts = g:vimDir.'/extra/scripts/init.vim'

" Carrega as configurações no vim de settings, mappings e plugins
exec ":source ".s:configSettings
exec ":source ".s:configMappings
exec ":source ".s:configPlugins
exec ":source ".s:configTheme
exec ":source ".s:extraSyntax
exec ":source ".s:extraScripts


