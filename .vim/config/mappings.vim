
" -----------------------------------------------------------
" [Mapeamento]

" leader
let g:mapleader = ","

" Abre MYVIMRC em um split
nnoremap <A-1> :split $MYVIMRC<cr>
" Abre settings.vim em um split
nnoremap <A-2> :split ~/.vim/config/settings.vim<cr>
" Abre mappings.vim em um split
nnoremap <A-3> :split ~/.vim/config/mappings.vim<cr>
" Abre plugins.vim em um split
nnoremap <A-4> :split ~/.vim/config/plugins.vim<cr>
" Abre plugins.vim em um split
nnoremap <A-5> :split ~/.vim/config/theme.vim<cr>
" Abre extra syntax
nnoremap <A-6> :split ~/.vim/extra/syntax/init.vim<cr>
" Abre extra scripts
nnoremap <A-7> :split ~/.vim/extra/scripts/init.vim<cr>


" Source MYVIMRC
nnoremap <A-0> :source $MYVIMRC<cr>
nnoremap <A-9> :source ~/.vim/config/theme.vim<cr>


" switch tab
nnoremap <S-l> :tabn<CR>
nnoremap <S-h> :tabp<CR>

" move tab
nnoremap <C-S-l> :tabmove<CR>

" insert mode uppercase the current word
"  <esc> : go to normal mode
"  v   : visual mode
"  iw    : select the current word
"  U   : uppercase selection
"  i   : back to insert mode
inoremap <c-u> <esc>viwUi


" remove last search highlight
nnoremap <C-l> :nohlsearch<CR><C-l>


" Wrap a word in double quotes
nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel

" Wrap a word in single quotes
nnoremap <leader>' viw<esc>a'<esc>hbi'<esc>lel

"Wrap a word with paretheses, brackets and
nnoremap <leader>) viw<esc>a)<esc>hbi(<esc>lel
nnoremap <leader>( viw<esc>a)<esc>hbi(<esc>lel

nnoremap <leader>] viw<esc>a]<esc>hbi[<esc>lel
nnoremap <leader>[ viw<esc>a]<esc>hbi[<esc>lel

nnoremap <leader>} viw<esc>a}<esc>hbi{<esc>lel
nnoremap <leader>{ viw<esc>a}<esc>hbi{<esc>lel


" select inside parents
onoremap in( :<c-u>normal! f(vi(<cr>

" select inside braces
onoremap in{ :<c-u>normal! f{vi{<cr>

" select inside brackets
onoremap in[ :<c-u>normal! f[vi[<cr>

" Leave insert mode (like <esc>) and disable <esc>
inoremap jk <esc>
" inoremap <special> <esc> <nop>
"inoremap <esc>^[ <esc>^[

" Disable arrow keys

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>

inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

nnoremap <Leader>b :ls<CR>:b<Space>

nnoremap <C-Tab> :b# <CR>
"nnoremap <C-S-Tab> :b# <CR>
nnoremap o o<Esc>
nnoremap O O<Esc>

" indent cursor
" normal mode
nnoremap <A-o> i.<Esc>==x<Esc>A
" insertmode
inoremap <A-o> .<Esc>==x<Esc>A

nnoremap <leader>o :only <CR>

inoremap <C-Space> <C-n>

inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-l> <Right>
cnoremap <C-h> <Left>
cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
cnoremap <C-l> <Right>


" Limpa ^M
nnoremap <leader>l :%s/\r//g<cr>

" New tab
nnoremap <leader>t :tabnew<cr>

" Close tab
nnoremap <leader>tc :tabclose<cr>

" Substitui texto pelo ultimo yank
map <C-k> ciw<C-r>0 <ESC>

" Godot
"nnoremap <buffer> <F4> :GodotRunLast<CR>
"nnoremap <buffer> <F5> :GodotRun<CR>
"nnoremap <buffer> <F6> :GodotRunCurrent<CR>
"nnoremap <buffer> <F7> :GodotRunFZF<CR>

map <leader>y "0y
map <leader>p "0p

" Calculadora
nnoremap <leader>c :Calculator<cr>

" FZF
nnoremap <A-f> :FZF<CR>

" Autoformat
noremap <F7> :Autoformat<CR>


if has("unix")
    function! FontSizePlus ()
      let l:gf_size_whole = matchstr(&guifont, '\( \)\@<=\d\+$')
      let l:gf_size_whole = l:gf_size_whole + 1
      let l:new_font_size = ' '.l:gf_size_whole
      let &guifont = substitute(&guifont, ' \d\+$', l:new_font_size, '')
    endfunction

    function! FontSizeMinus ()
      let l:gf_size_whole = matchstr(&guifont, '\( \)\@<=\d\+$')
      let l:gf_size_whole = l:gf_size_whole - 1
      let l:new_font_size = ' '.l:gf_size_whole
      let &guifont = substitute(&guifont, ' \d\+$', l:new_font_size, '')
    endfunction
else
    function! FontSizePlus ()
      let l:gf_size_whole = matchstr(&guifont, '\(:h\)\@<=\d\+$')
      let l:gf_size_whole = l:gf_size_whole + 1
      let l:new_font_size = ':h'.l:gf_size_whole
      let &guifont = substitute(&guifont, ':h\d\+$', l:new_font_size, '')
    endfunction

    function! FontSizeMinus ()
      let l:gf_size_whole = matchstr(&guifont, '\(:h\)\@<=\d\+$')
      let l:gf_size_whole = l:gf_size_whole - 1
      let l:new_font_size = ':h'.l:gf_size_whole
      let &guifont = substitute(&guifont, ':h\d\+$', l:new_font_size, '')
    endfunction
endif


if has("gui_running")
    nmap <S-F12> :call FontSizeMinus()<CR>
    nmap <F12> :call FontSizePlus()<CR>
endif

" Auto indent on Enter
"imap <C-Return> <CR><CR><C-o>k<Tab>


nnoremap <A-S-j> :m .+1<CR>==
nnoremap <A-S-k> :m .-2<CR>==
inoremap <A-S-j> <Esc>:m .+1<CR>==gi
inoremap <A-S-k> <Esc>:m .-2<CR>==gi
vnoremap <A-S-j> :m '>+1<CR>gv=gv
vnoremap <A-S-k> :m '<-2<CR>gv=gv


" Terminal

function Execute()
    let extension = expand('%:e')
    if match(extension,"js") == 0 || match(extension,"node") == 0
        execute ":FloatermSend node %"
    elseif match(extension, "php") == 0
        execute ":FloatermSend php %"
    elseif match(extension, "sce") == 0
        execute ":FloatermSend exec('%',-1)"
    elseif match(extension, "py") == 0
        execute ":FloatermSend python %"
    else
        execute ":FloatermSend run %"
    endif
endfunction

function Clear()
    let extension = expand('%:e')
    if match(extension, "sce") == 0
        execute ":FloatermSend clc"
    else
        execute ":FloatermSend clear"
    endif
endfunction

nnoremap   <silent>   <F2>    :FloatermNew --wintype=vsplit<CR>
tnoremap   <silent>   <F2>    <C-\><C-n>:FloatermNew --wintype=vsplit<CR>
nnoremap   <silent>   <F3>    :FloatermPrev<CR>
tnoremap   <silent>   <F3>    <C-\><C-n>:FloatermPrev<CR>
nnoremap   <silent>   <F4>    :FloatermNext<CR>
tnoremap   <silent>   <F4>    <C-\><C-n>:FloatermNext<CR>
nnoremap   <silent>   <f5>   :FloatermToggle<cr>
tnoremap   <silent>   <f5>   <c-\><c-n>:FloatermToggle<cr>

"nnoremap <F2> :FloatermNew<CR>
"nnoremap <F3> :FloatermNext<CR>

nnoremap   <silent>   <f6>   :FloatermKill<cr>
tnoremap   <silent>   <f6>   <c-\><c-n>:FloatermKill<cr>
nnoremap <F8> :call Execute()<CR>
nnoremap <F9> :call Clear()<CR>


" Automatically closing braces
inoremap {<CR> {<CR>}<Esc>ko
inoremap [<CR> [<CR>]<Esc>ko
inoremap (<CR> (<CR>)<Esc>ko

" NERDTree
map <leader>a :NERDTreeToggle %<CR>


" Copy / Paste outside Vim
nnoremap <C-y> "+y
vnoremap <C-y> "+y
nnoremap <C-p> "+p
vnoremap <C-p> "+


" resize window
nnoremap <silent> <Leader>= :exe "vertical resize +10" <CR>
nnoremap <silent> <Leader>- :exe "vertical resize -10" <CR>
