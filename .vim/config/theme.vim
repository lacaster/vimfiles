
" mostra linha ativa
set cursorline

" força background azulado na linha ativa
hi CursorLine cterm=NONE ctermbg=darkblue ctermfg=white guibg=#1f212d ctermfg=white

" visual mode com linha azulzinha
hi Visual term=reverse cterm=reverse guibg=#2c2f3f
" muda cor do cursor
highlight Cursor guifg=#000000 guibg=#fff42b

" Muda cor das tabs
let s:palette = g:lightline#colorscheme#powerline#palette
let s:palette.tabline.left =  [ [ '#d9dadf', '#2c2f3f', 252, 66 ] ]
let s:palette.tabline.tabsel = [ [ '#ffffff', '#000000', 252, 66, 'bold' ] ]
let s:palette.tabline.middle = [ [ '#d9dadf', '#000000', 252, 66 ] ]
let s:palette.tabline.right = [ [ '#d9dadf', '#2c2f3f', 252, 66 ] ]
unlet s:palette

" Extra bar git and debug
highlight SignColumn guibg=#111111 ctermbg=NONE


let s:configMappings = $HOME.'/.vim/config/mappings.vim'
exec ":source ".s:configMappings
