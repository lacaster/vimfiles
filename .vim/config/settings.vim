
" ----------------------------------------------------------
" [Setting]


if ! has("gui_running") " se estiver rodando gvim
    set t_Co=256        " habilita 256 cores antes
endif                   " de definir o esquema de cores



" esquema de cores
colorscheme monokai-phoenix

" font e tamanho
set guifont=IBM\ Plex\ Mono\ Medium\ 10

"highlight ColorColumn ctermbg=0 guibg=DarkGrey
set fillchars=

" quebra final de linha
set wrap

" mostra número na linha
set number





" diminui numberwidth
set numberwidth=1

" realça sintaxe
syntax on


" identação
set smartindent " adiciona um nível extra de identação dependendo do contexto
set autoindent " copia a identação da linha anterior
set copyindent " preserva identacão
set shiftwidth=4 " identação possui 4 espaços
set shiftround " habilita identacao com as teclas << e >> no modo normal
set backspace=indent,eol,start " backspace passa a funcionar como outros programas
set smarttab " usa shiftwidth ao invés da pausa do tab
set expandtab " Troca 4 espaços por tab

" busca
set showmatch " marca visual no resultado da busca no texto
set hlsearch " destaca todas as resultados da busca
set incsearch " destaca o texto ao realizar a busca

" copy/paste
set paste " cola texto com mesma formatação que foi copiado
set clipboard=unnamedplus " usa clipboard do sistema

" alinhamento manual
set foldmethod=manual

" mouse
set mouse= "a mouse ativo em todos os modos do vim

set guioptions-=m  "remove barra de menu
set guioptions-=T  "remove barra de ferramentas
set guioptions-=L  "remove barra de rolagem

set wildmenu       "completar na linha de comando

set linespace=11 " espaçamento entre as linhas

set history=1000 " quantidade de comandos histórico
set showcmd " mostra comandos na barra de status ao ser digitado
set noswapfile " desativa swap file
"set nonu rnu "número relativo de linha

set enc=utf-8 "codificação utf-8
set fileencoding=utf-8 "codificação do arquivo utf-8

set hidden "esconde buffers ao invés de fechá-los

runtime macros/matchit.vim


" (Folding)
set foldmethod=manual
set nofoldenable

set path+=**

" prettier
" number of spaces per indentation level
let g:prettier#config#tab_width = 4

let g:tex_flavor = 'latex'

" Trigger configuration. You need to change this to something else than <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

set clipboard=unnamed

set guioptions=c


" disable insert (paste) mode
set nopaste

autocmd BufNewFile,BufRead *.sage set syntax=sage
autocmd BufNewFile,BufRead *.sce set syntax=scilab
autocmd BufNewFile,BufRead *.nlogo set syntax=netlogo


if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window.
  set lines=999 columns=999
endif

set omnifunc=syntaxcomplete#Complete

" Enable omni completion per file.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }

" VimWiki
let g:vimwiki_list = [{'path': '~/Documentos/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
