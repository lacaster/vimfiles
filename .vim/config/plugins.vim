
" --------------------------------------------------
" [Plugins]
"
" ( NERDTree )
" on vim enter opens nerd tree

let g:NERDTreeChDirMode=2
nnoremap <leader>n :NERDTreeToggle <CR>

function! OpenNerdTree()
    let s:exclude = ['COMMIT_EDITMSG', 'MERGE_MSG']
    if index(s:exclude, expand('%:t')) < 0
        NERDTreeFind
        exec "normal! \<c-w>\<c-w>"
    endif
endfunction
autocmd VimEnter * call OpenNerdTree()


" redimensiona janela do nerd tree
let g:NERDTreeWinSize = 35

" mostra arquivos ocultos
let g:NERDTreeShowHidden=1

" ignora arquivos .swp
let g:NERDTreeIgnore=['\.swp$', '\~$']
nnoremap <A-n> :NERDTreeToggle<cr>

" ajuda ao sair quando não existem buffers abertos além do NerdTree
function! CheckLeftBuffers()
    if tabpagenr('$') == 1
        let i = 1
        while i <= winnr('$')
            if getbufvar(winbufnr(i), '&buftype') == 'help' ||
                        \ getbufvar(winbufnr(i), '&buftype') == 'quickfix' ||
                        \ exists('t:NERDTreeBufName') &&
                        \   bufname(winbufnr(i)) == t:NERDTreeBufName ||
                        \ bufname(winbufnr(i)) == '__Tag_List__'
                let i += 1
            else
                break
            endif
        endwhile
        if i == winnr('$') + 1
            qall
        endif
        unlet i
    endif
endfunction
autocmd BufEnter * call CheckLeftBuffers()

" Atualiza NERD Tree para arquivo atual
map <leader>r :NERDTree %<cr> :lcd %:p:h<cr>



" ( multiple cursor ]

" let g:multi_cursor_use_default_mapping=0
" let g:multi_cursor_next_key='<C-m>'
" let g:multi_cursor_select_all_word_key='<A-m>'
" let g:multi_cursor_prev_key='<C-p>'
" let g:multi_cursor_skip_key='<C-x>'
" let g:multi_cursor_quit_key='<esc>'

" ( Visual Multi)
" let g:VM_leader = ','
" let g:VM_maps = {}
" let g:VM_maps['Find Under'] = '<C-m>'
" let g:VM_maps['Find Subword Under'] = '<C-m>'
" let g:VM_maps["Add Cursor Down"] = '<C-m>'
" let g:VM_maps["Select All"] = '<A-m>'

let g:VM_maps = {}
let g:VM_maps['Find Under']         = '<C-m>'           " replace C-n
let g:VM_maps['Find Subword Under'] = '<C-m>'


" (Goyo)
nnoremap <leader>g :Goyo<CR> :source ~/.vim/config/theme.vim<cr>
nnoremap <leader>gg :Goyo 120<CR> :source ~/.vim/config/theme.vim<cr>

" (Nerd commenter)
"noremap <c-_> :call NERDComment('0','toggle')<cr>

" (Ctrl+p)
nnoremap <M-p> :CtrlPBuffer <CR>

" Emmet
"let g:user_emmet_leader_key='<C-e>'
"let g:user_emmet_expandabbr_key = '<C-e>,'

imap   <A-e>   <plug>(emmet-expand-abbr)
" Syntastic
let g:syntastic_javascript_checkers=['eslint']


" SplitLine
nnoremap <A-r> :SplitLine<CR>

" Latex
let g:livepreview_previewer = 'evince'
let g:livepreview_engine = 'pdflatex'

" Syntastic
let g:syntastic_java_javac_classpath = '/home/lacaster/Programas/java-jars/algs4/algs4.jar:./'

