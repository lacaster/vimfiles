let g:vimDir = $HOME.'/.vim'

" let s:syntaxScilab = g:vimDir.'/extra/syntax/scilab.vim'
let s:syntaxNetlogo = g:vimDir.'/extra/syntax/netlogo.vim'

" exec ":source ".s:syntaxScilab
exec ":source ".s:syntaxNetlogo


nnoremap o o<Esc>
nnoremap O O<Esc>
