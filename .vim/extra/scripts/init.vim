let g:vimDir = $HOME.'/.vim'

let s:extraRename = g:vimDir.'/extra/scripts/rename.vim'

exec ":source ".s:extraRename
